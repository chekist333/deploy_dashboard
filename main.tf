terraform {
  
  required_providers {
    grafana = {
      source = "grafana/grafana"
       
    }
  }
  
}

provider "grafana" {
    auth = var.auth
    url = var.base_url
}


data "grafana_folder" "deploy_folder" {
  title = "Test_folder"
}

resource "grafana_dashboard" "deploy_folder" {
  folder      = data.grafana_folder.deploy_folder.id
  config_json = file("Docker.json")

}