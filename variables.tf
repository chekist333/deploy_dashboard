variable "auth" {
  type        = string
  description = "GitLab personal access token"
  sensitive   = true
}

variable "base_url" {
  type = string
  default = "https://grafana.k8s.abagy.com/"
}